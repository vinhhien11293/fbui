import 'dart:convert';
import '../client/dio_client.dart';

class ServiceResponse {
  static const errorUnknown = "unknown";
  static const errorParsing = "ParseError";

  dynamic res;
  String? err;
  List<dynamic> additionData;
  bool isSuccess = false;

  ServiceResponse({this.res, this.err, this.additionData = const []}) {
    isSuccess = err == null ? true : false;
  }

  static ServiceResponse processJsonResponse(DioResponse dioResponse, {required dynamic Function(Map<String, dynamic>) parse, List Function(Map<String, dynamic>)? parseAdditional}) {
    if(dioResponse.isSuccess) {
      try {
        Map<String, dynamic> response = jsonDecode(dioResponse.data);
        if(response['success']) {
          return ServiceResponse(res: parse(response['data']), additionData: parseAdditional?.call(response['data']) ?? []);
        } else {
          return ServiceResponse(err: response['message'] ?? "");
        }
      } catch (err) {
        return ServiceResponse(err: ServiceResponse.errorParsing);
      }
    } else {
      return ServiceResponse(err: dioResponse.err?.message ?? "");
    }
  }

  static ServiceResponse processJsonArrayResponse(DioResponse dioResponse, {required dynamic Function(List<dynamic>) parse, List Function(List<dynamic>)? parseAdditional}) {
    if(dioResponse.isSuccess) {
      try {
        Map<String, dynamic> response = jsonDecode(dioResponse.data);
        if(response['success']) {
          return ServiceResponse(res: parse(response['data']), additionData: parseAdditional?.call(response['data']) ?? []);
        } else {
          return ServiceResponse(err: response['message'] ?? "");
        }
      } catch (err) {
        return ServiceResponse(err: ServiceResponse.errorParsing);
      }
    } else {
      return ServiceResponse(err: dioResponse.err?.message ?? "");
    }
  }

  static ServiceResponse processStringResponse(DioResponse dioResponse) {
    if(dioResponse.isSuccess) {
      try {
        Map<String, dynamic> response = jsonDecode(dioResponse.data);
        if(response['success']) {
          return ServiceResponse(res: response['data']);
        } else {
          return ServiceResponse(err: response['message'] ?? "");
        }
      } catch (err) {
        return ServiceResponse(err: ServiceResponse.errorParsing);
      }
    } else {
      return ServiceResponse(err: dioResponse.err?.message ?? "");
    }
  }
}