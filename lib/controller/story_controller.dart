import 'package:get/get.dart';

import '../widgets/story/story_item.dart';

class StoryController extends GetxController {
  var rxStories = <StoryData>[].obs;

  getStories() {
    rxStories.add(StoryData(
      name: "",
      avatarUrl: "",
      imageUrl: "https://scontent.fhan2-1.fna.fbcdn.net/v/t39.30808-6/279816791_4542936262474708_278005600590831703_n.jpg?stp=dst-jpg_s1080x2048&_nc_cat=1&ccb=1-6&_nc_sid=2c4854&_nc_ohc=GwBLq4--hKYAX9Jph7U&tn=0-DvzHOp3HML_x6s&_nc_ht=scontent.fhan2-1.fna&oh=00_AT__2QkhuhY_LL_LFlx8KZI9bd4VY_cPdfn1WPS7AoZBgQ&oe=6285B748",
      isCreator: true
    ));
    rxStories.add(StoryData(
      name: "Nam Phương",
      imageUrl: "https://scontent.fpnh22-3.fna.fbcdn.net/v/t39.30808-6/280616624_2270669826416336_2153947944872940272_n.jpg?stp=dst-jpg_p320x320&_nc_cat=109&ccb=1-6&_nc_sid=365331&_nc_ohc=c8eMK0kJoKQAX8Jl-WW&_nc_oc=AQm0oPkn_X-corLD50zyXFtRNfzwtaDlCIMY2HWj5EbLQTkbykG6pB80xgeEFhIAXDU&_nc_ht=scontent.fpnh22-3.fna&oh=00_AT-GjPvUYN10a4Gh5Z7ahJsP0nQ8Ij_iSpUF6T0P_fMSAw&oe=628561C8",
      avatarUrl: "https://scontent.fpnh22-1.fna.fbcdn.net/v/t39.30808-1/278942437_5259688224076888_1320996696116561161_n.jpg?stp=dst-jpg_p100x100&_nc_cat=101&ccb=1-6&_nc_sid=7206a8&_nc_ohc=nnTxwpcCogUAX8WbFaQ&_nc_ht=scontent.fpnh22-1.fna&oh=00_AT8zrbniutX24F3kKehfM3eLy2x4LxJ18nkyBV_oJod1Mg&oe=6286496A",
      isCreator: false
    ));
    rxStories.add(StoryData(
        name: "Nguyễn Văn Giang",
        imageUrl: "https://scontent.fpnh22-2.fna.fbcdn.net/v/t15.5256-10/279769696_973073743353896_640752974643122093_n.jpg?stp=dst-jpg_p320x320&_nc_cat=111&ccb=1-6&_nc_sid=ad6a45&_nc_ohc=RFRRwZWsh9QAX-L1t8K&_nc_ht=scontent.fpnh22-2.fna&oh=00_AT899vHUKTbA6AyHMLualkk_dl1_M-WoXcpx2sxREDn84A&oe=6285877E",
        avatarUrl: "https://scontent.fpnh22-3.fna.fbcdn.net/v/t39.30808-1/270846154_2262211387249946_2697543768043895944_n.jpg?stp=dst-jpg_p100x100&_nc_cat=102&ccb=1-6&_nc_sid=7206a8&_nc_ohc=1UniHaIEjO0AX_a9CVX&tn=0-DvzHOp3HML_x6s&_nc_ht=scontent.fpnh22-3.fna&oh=00_AT_iBBGv9rPxfJfGb-irHcVUMDmG2JCtTohLGxJKKls74g&oe=6285294C",
        isCreator: false
    ));
    rxStories.add(StoryData(
        name: "Nam Phương",
        imageUrl: "https://scontent.fpnh22-3.fna.fbcdn.net/v/t39.30808-6/280616624_2270669826416336_2153947944872940272_n.jpg?stp=dst-jpg_p320x320&_nc_cat=109&ccb=1-6&_nc_sid=365331&_nc_ohc=c8eMK0kJoKQAX8Jl-WW&_nc_oc=AQm0oPkn_X-corLD50zyXFtRNfzwtaDlCIMY2HWj5EbLQTkbykG6pB80xgeEFhIAXDU&_nc_ht=scontent.fpnh22-3.fna&oh=00_AT-GjPvUYN10a4Gh5Z7ahJsP0nQ8Ij_iSpUF6T0P_fMSAw&oe=628561C8",
        avatarUrl: "https://scontent.fpnh22-1.fna.fbcdn.net/v/t39.30808-1/278942437_5259688224076888_1320996696116561161_n.jpg?stp=dst-jpg_p100x100&_nc_cat=101&ccb=1-6&_nc_sid=7206a8&_nc_ohc=nnTxwpcCogUAX8WbFaQ&_nc_ht=scontent.fpnh22-1.fna&oh=00_AT8zrbniutX24F3kKehfM3eLy2x4LxJ18nkyBV_oJod1Mg&oe=6286496A",
        isCreator: false
    ));
  }
}