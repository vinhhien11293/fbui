import 'package:fbui/constants/resources.dart';
import 'package:fbui/widgets/avatar/avatar.dart';
import 'package:flutter/material.dart';

class PostCreator extends StatelessWidget {
  const PostCreator({Key? key, required this.avatarUrl, required this.avatarStatus}) : super(key: key);

  final String avatarUrl;
  final String avatarStatus;

  @override
  Widget build(BuildContext context) {
    return Container(
      color: Colors.white,
      padding: const EdgeInsets.symmetric(vertical: 20, horizontal: 15),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Row(
            mainAxisSize: MainAxisSize.min,
            children: [
              Avatar(
                size: AvatarSize.medium,
                avatarStatusUrl: avatarStatus,
                avatarUrl: avatarUrl,
              ),
              const SizedBox(width: 20,),
              Text("What's on your mind", style: Theme.of(context).textTheme.bodyMedium?.copyWith(fontWeight: FontWeight.bold),)
            ],
          ),
          Image.asset(kImageImage, width: 40, fit: BoxFit.fitWidth,)
        ],
      ),
    );
  }
}