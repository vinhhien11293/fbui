import 'dart:ffi';
import 'package:dio/dio.dart';
import 'package:get/get.dart' hide Response;
import 'package:get_storage/get_storage.dart';
import 'package:rxdart/rxdart.dart';
import '../config/config.dart';

class DioClient {
  static const keyAccessToken = "access_token";
  static const methodGet = "GET";
  static const methodPost = "POST";

  PublishSubject event401 = PublishSubject<Void>();
  final GetStorage _getStorage = Get.find();

  final Dio dio = Dio(BaseOptions(
    baseUrl: kBaseUrl,
    connectTimeout: kRequestTimeout * 1000,
    receiveTimeout: kRequestTimeout * 1000,
    contentType: "application/json",
    responseType: ResponseType.plain,
    headers: {
      "Accept": "*/*",
      "Connection": "keep-alive",
    }
  ));

  Future<DioResponse> request(String url, {dynamic body, Map<String, dynamic> params = const {}, Map<String, dynamic> headers = const {}, String method = methodGet, bool useToken = true, bool useLang = true}) async {
    if(headers.isEmpty) {
      headers = {};
    }
    if(params.isEmpty) {
      params = {};
    }

    if(useToken) {
      String token = _getStorage.read<String>(keyAccessToken) ?? "";
      if(token.isNotEmpty) {
        headers['authorization'] = "Bearer $token";
      }
    }

    try {
      Response res = await dio.request(
          url,
          queryParameters: params,
          data: body,
          options: Options(
              method: method,
              headers: headers
          )
      );
      return DioResponse(data: res.data.toString());
    } on DioError catch(e) {
      if(e.response?.statusCode == 401) {
        event401.sink.add(null);
      }
      return DioResponse(err: e);
    }
  }
}

class DioResponse {
  String data;
  DioError? err;
  bool isSuccess = false;

  DioResponse({this.data = "", this.err}) {
    isSuccess = err == null ? true : false;
  }
}
