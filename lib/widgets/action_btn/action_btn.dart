import 'package:flutter/material.dart';
import '../../constants/resources.dart';

class _ActionBtn extends StatelessWidget {
  const _ActionBtn({Key? key, required this.title, required this.icon, this.onTap}) : super(key: key);

  final String icon;
  final String title;
  final Function()? onTap;

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () {
        onTap?.call();
      },
      child: Container(
        decoration: BoxDecoration(
            color: Colors.white,
            borderRadius: BorderRadius.circular(20),
            border: Border.all(color: Colors.black12, width: 1)
        ),
        padding: const EdgeInsets.symmetric(vertical: 8, horizontal: 15),
        child: Row(
          mainAxisSize: MainAxisSize.min,
          children: [
            Image.asset(icon, width: 20, height: 20,),
            const SizedBox(width: 10,),
            Text(title, style: Theme.of(context).textTheme.labelLarge?.copyWith(fontWeight: FontWeight.bold))
          ],
        ),
      ),
    );
  }
}

class ActionBtns extends StatelessWidget {
  const ActionBtns({Key? key, this.onGroup, this.onReel, this.onRoom, this.onStatus}) : super(key: key);

  final Function()? onReel;
  final Function()? onRoom;
  final Function()? onStatus;
  final Function()? onGroup;

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: const BoxDecoration(
        color: Color(0x4de7e7e7)
      ),
      padding: const EdgeInsets.symmetric(vertical: 10),
      child: SingleChildScrollView(
        scrollDirection: Axis.horizontal,
        child: Row(
          children: [
            const SizedBox(width: 20,),
            _ActionBtn(title: "Reel", icon: kImageReel, onTap: onReel,),
            const SizedBox(width: 20,),
            _ActionBtn(title: "Room", icon: kImageRoom, onTap: onRoom,),
            const SizedBox(width: 20,),
            _ActionBtn(title: "Status", icon: kImageStatus, onTap: onStatus,),
            const SizedBox(width: 20,),
            _ActionBtn(title: "Group", icon: kImageGroup, onTap: onGroup,),
            const SizedBox(width: 20,),
          ],
        ),
      ),
    );
  }
}