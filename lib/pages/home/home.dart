
import 'package:fbui/controller/post_controller.dart';
import 'package:fbui/controller/story_controller.dart';
import 'package:fbui/widgets/action_btn/action_btn.dart';
import 'package:fbui/widgets/post/post_creator.dart';
import 'package:fbui/widgets/post/posts.dart';
import 'package:fbui/widgets/story/stories.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import '../../constants/resources.dart';
import '../../widgets/bottombar/bottombar.dart';

class HomeScreen extends StatefulWidget {
  const HomeScreen({Key? key}) : super(key: key);

  @override
  State<StatefulWidget> createState() {
    return _HomeScreenState();
  }
}

class _HomeScreenState extends State<HomeScreen> with TickerProviderStateMixin {

  int currentBottomNavIndex = 0;
  late TabController _tabController;
  late TabController _tabControllerStory;

  final PostController _postController = Get.find();
  final StoryController _storyController = Get.find();

  @override
  void initState() {
    super.initState();
    _tabController = TabController(length: 3, vsync: this);
    _tabControllerStory = TabController(length: 3, vsync: this);
  }

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();

    _postController.getPost();
    _storyController.getStories();
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        body: ListView(
          shrinkWrap: true,
          children: [
            Row(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Expanded(
                  child: TabBar(
                    controller: _tabController,
                    tabs: const [
                      Tab(text: "Your Feed",),
                      Tab(text: "Favorites",),
                      Tab(text: "Recent",),
                    ],
                  ),
                ),
                const SizedBox(width: 20,),
                Image.asset(kImageSetting, width: 20, fit: BoxFit.fitWidth,),
                const SizedBox(width: 20,)
              ],
            ),
            const PostCreator(
              avatarUrl: "https://scontent.fhan4-3.fna.fbcdn.net/v/t1.6435-1/80367395_2564045300381682_2043101528796954624_n.jpg?stp=dst-jpg_p100x100&_nc_cat=103&ccb=1-6&_nc_sid=7206a8&_nc_ohc=l4iNvghSnTkAX_epARQ&_nc_ht=scontent.fhan4-3.fna&oh=00_AT81WLYC-Ht4XV7OSK9Y4hFL-hWC302b26FkVLRSFcf6_w&oe=62A56F63",
              avatarStatus: kImageStatus,
            ),
            const ActionBtns(),
            Container(
              height: 5,
              decoration: const BoxDecoration(
                color: Colors.black12
              ),
            ),
            Row(
              children: [
                Expanded(
                  child: TabBar(
                    controller: _tabControllerStory,
                    tabs: const [
                      Tab(text: "Story",),
                      Tab(text: "Reel",),
                      Tab(text: "Room",)
                    ],
                  ),
                )
              ],
            ),
            Stories(),
            Posts(),
            Container(
              height: 250,
              decoration: const BoxDecoration(
                  color: Colors.black12
              ),
            ),
          ]
        ),
        bottomNavigationBar: Column(
          mainAxisSize: MainAxisSize.min,
          children: [
            AnimatedContainer(
              duration: const Duration(milliseconds: 100),
              width: double.maxFinite,
              alignment: Alignment.centerLeft,
              padding: EdgeInsets.only(left: MediaQuery.of(context).size.width/5*currentBottomNavIndex),
              child: Container(
                width: MediaQuery.of(context).size.width/5,
                height: 3,
                decoration: BoxDecoration(
                    color: Theme.of(context).primaryColor,
                    borderRadius: BorderRadius.circular(10)
                ),
              ),
            ),
            Container(
              height: 1,
              color: Colors.black12,
            ),
            BottomBar(
              currentIndex: currentBottomNavIndex,
              images: const [kImageHome, kImageWatch, kImageMarket, kImageDating, kImageNotification],
              labels: const ["Home", "Watch", "Marketplace", "Dating", "Notification"],
              onChange: (index) {
                setState(() {
                  currentBottomNavIndex = index;
                });
              },
            )
          ],
        ),
      ),
    );
  }

}