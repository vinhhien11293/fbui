
import 'package:fbui/controller/post_controller.dart';
import 'package:fbui/widgets/post/post.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class Posts extends StatelessWidget {
  final PostController _postController = Get.find();

  Posts({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Obx(
        () => ListView.builder(
          shrinkWrap: true,
          physics: const ClampingScrollPhysics(),
          itemCount: _postController.rxPostData.length,
          itemBuilder: (BuildContext context, int index) => Column(
            mainAxisSize: MainAxisSize.min,
            children: [
              Obx(() => Post(postData: _postController.rxPostData[index]),),
              Container(
                height: 30,
                color: Colors.black12,
              )
            ],
          ),
        )
    );
  }
}