
const kImageHome = "assets/images/home.svg";
const kImageWatch = "assets/images/video.svg";
const kImageMarket = "assets/images/market.svg";
const kImageDating = "assets/images/date.svg";
const kImageNotification = "assets/images/notifications.svg";

const kImageReel = "assets/images/reels.png";
const kImageRoom = "assets/images/room.png";
const kImageStatus = "assets/images/face.png";
const kImageGroup = "assets/images/group.png";
const kImageImage = "assets/images/image.png";
const kImageSetting = "assets/images/setting.png";
const kImageEarth = "assets/images/earth.png";

const kImageCare = "assets/images/care.png";
const kImageLike = "assets/images/like.png";
const kImageHaha = "assets/images/haha.png";
const kImageSad = "assets/images/sad.png";
const kImageLove = "assets/images/love.png";
const kImageVow = "assets/images/wow.png";
const kImageAngry = "assets/images/angry.png";

const kImageRLike = "assets/images/fblike.png";
const kImageRComment = "assets/images/comment.png";
const kImageRshare = "assets/images/share.png";