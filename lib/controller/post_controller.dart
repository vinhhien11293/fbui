import 'package:fbui/services/service_api.dart';
import 'package:fbui/services/service_response.dart';
import 'package:get/get.dart';
import '../model/post.dart';
import '../widgets/post/post.dart';

class PostController extends GetxController {
  final ServiceApi _serviceApi = Get.find();
  var rxPostData = <PostData>[].obs;

  Future<ServiceResponse> getPost() async {
    ServiceResponse serviceResponse = await _serviceApi.getPost();
    if(serviceResponse.isSuccess) {
      rxPostData.clear();
      rxPostData.addAll((serviceResponse.res as List<PostModel>).map((e) => PostData(
        title: e.title,
        subTitle: e.name,
        time: e.time,
        content: e.content,
        contentColor: e.color,
        avatarUrl: e.avatar,
        imageUrl: e.contentImage,
        bodyType: e.bodyType == 1 ? BodyType.textImage : BodyType.textOnly,
        headerType: e.headType == 1 ? HeaderType.people : HeaderType.group,
        footerType: e.footType == 1 ? FooterType.likeShareComment : FooterType.likeCommentMessage,
        totalComment: e.totalComment,
        totalReaction: e.totalReact,
        reacts: e.reacts.map((e) {
          switch (e) {
            case 0:
              return ReactionType.care;
            case 1:
              return ReactionType.haha;
            case 2:
              return ReactionType.love;
            case 3:
              return ReactionType.sad;
            case 4:
              return ReactionType.angry;
            case 5:
              return ReactionType.wow;
            case 6:
              return ReactionType.like;
          }

          return ReactionType.haha;
        }).toList()
      )).toList());
    }
    return serviceResponse;
  }
}