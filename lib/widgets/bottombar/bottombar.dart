import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';

class BottomBar extends StatelessWidget {
   BottomBar({Key? key, required this.images, required this.labels, required this.currentIndex, this.onChange}) : super(key: key) {
     if(images.length > 5 || images.length < 3) {
       throw Exception("invalid item range <3-5>");
     }

     if(labels.length != images.length) {
       throw Exception("images and label must have same length");
     }
   }

   final List<String> images;
   final List<String> labels;
   final Function(int)? onChange;
   final int currentIndex;

  _icon(String path, BuildContext context, Color? color) {
    return Container(
      padding: const EdgeInsets.only(bottom: 5),
      width: 25,
      height: 25,
      child: SvgPicture.asset(path, color: color),
    );
  }

  List<BottomNavigationBarItem> _getItem(BuildContext context) {
    List<BottomNavigationBarItem> items = [];
    for(int i=0; i<images.length; i++) {
      items.add(BottomNavigationBarItem(
          label: labels[i],
          icon: _icon(images[i], context, currentIndex == i ? Theme.of(context).bottomNavigationBarTheme.selectedItemColor : Theme.of(context).bottomNavigationBarTheme.unselectedItemColor)
      ));
    }

    return items;
  }

  @override
  Widget build(BuildContext context) {
    return BottomNavigationBar(
      selectedFontSize: 10,
      unselectedFontSize: 10,
      currentIndex: currentIndex,
      items: _getItem(context),
      onTap: (index) {
        onChange?.call(index);
      },
    );
  }
}