
import 'package:cached_network_image/cached_network_image.dart';
import 'package:fbui/widgets/avatar/avatar.dart';
import 'package:flutter/material.dart';

class StoryData {
  String name;
  String avatarUrl;
  String imageUrl;
  bool isCreator;

  StoryData({
    required this.imageUrl, required this.name, required this.avatarUrl, required this.isCreator
});
}

class StoryItem extends StatelessWidget {
  const StoryItem({Key? key, required this.storyImage, required this.storyName, required this.storyAvatar, this.isMe = false}) : super(key: key);

  final String storyImage;
  final String storyName;
  final String storyAvatar;
  final bool isMe;

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width: 120,
      height: 190,
      child: Stack(
        children: [
          Positioned.fill(
            child: ClipRRect(
              borderRadius: BorderRadius.circular(10),
              child: CachedNetworkImage(
                imageUrl: storyImage,
                fit: BoxFit.fitWidth,
              ),
            ),
          ),
          Positioned.fill(
            child: ClipRRect(
              borderRadius: BorderRadius.circular(10),
              child: Stack(
                children: [
                  Container(
                    decoration: const BoxDecoration(
                        color: Colors.black12
                    ),
                  ),
                  isMe ? Align(
                    alignment: Alignment.bottomCenter,
                    child: FractionallySizedBox(
                      heightFactor: 0.5,
                      child: Stack(
                        alignment: Alignment.topCenter,
                        children: [
                          Container(
                            decoration: const BoxDecoration(
                                color: Color(0xffdcdbd9)
                            ),
                            margin: const EdgeInsets.only(top: 30),
                          ),
                          Positioned(
                            child: Container(
                              width: 60,
                              height: 60,
                              decoration: const BoxDecoration(
                                color: Color(0xffdcdbd9),
                                shape: BoxShape.circle
                              ),
                              alignment: Alignment.center,
                              child: Container(
                                width: 50,
                                height: 50,
                                decoration: BoxDecoration(
                                    color: Theme.of(context).primaryColor,
                                    shape: BoxShape.circle
                                ),
                                child: Stack(
                                  children: [
                                    Positioned.fill(
                                      child: Container(
                                        alignment: Alignment.center,
                                        padding: const EdgeInsets.symmetric(horizontal: 7),
                                        child: Container(
                                          height: 5,
                                          decoration: BoxDecoration(
                                            color: Colors.white,
                                            borderRadius: BorderRadius.circular(10)
                                          ),
                                        ),
                                      ),
                                    ),
                                    Positioned.fill(
                                      child: Container(
                                        alignment: Alignment.center,
                                        padding: const EdgeInsets.symmetric(vertical: 7),
                                        child: Container(
                                          width: 5,
                                          decoration: BoxDecoration(
                                              color: Colors.white,
                                              borderRadius: BorderRadius.circular(10)
                                          ),
                                        ),
                                      ),
                                    )
                                  ],
                                ),
                              ),
                            ),
                          )
                        ],
                      ),
                    ),
                  ) : Container()
                ],
              ),
            ),
          ),
          Align(
            alignment: Alignment.bottomLeft,
            child: Container(
              padding: const EdgeInsets.symmetric(vertical: 10, horizontal: 10),
              child: Text(
                storyName,
                style: Theme.of(context).textTheme.labelLarge?.copyWith(fontWeight: FontWeight.bold, color: Colors.white),
              ),
            ),
          ),
          isMe ? Container() : Align(
            alignment: Alignment.topLeft,
            child: Container(
              padding: const EdgeInsets.all(5),
              child: Avatar(
                avatarUrl: storyAvatar,
                isBorder: true,
              ),
            ),
          ),
        ],
      ),
    );
  }
}