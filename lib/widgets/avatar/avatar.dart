
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';

import '../../constants/resources.dart';

enum AvatarSize {
  small, medium, large
}

class Avatar extends StatelessWidget {
  Avatar({Key? key, this.size = AvatarSize.small, required this.avatarUrl, this.avatarStatusUrl = "", this.isBorder = false }) : super(key: key) {
    if(size == AvatarSize.small) {
      realSize = 40;
    }
    if(size == AvatarSize.medium) {
      realSize = 50;
    }
    if(size == AvatarSize.large) {
      realSize = 70;
    }
  }

  double realSize = 0;
  final bool isBorder;
  final AvatarSize size;
  final String avatarUrl;
  final String avatarStatusUrl;

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: isBorder ? EdgeInsets.all(realSize/10) : const EdgeInsets.all(0),
      decoration: BoxDecoration(
        color: Theme.of(context).primaryColor,
        shape: BoxShape.circle
      ),
      child: Stack(
          children: [
            ClipOval(
              child: CachedNetworkImage(
                  width: realSize,
                  height: realSize,
                  placeholder: (context, url) => const CircularProgressIndicator(),
                  errorWidget: (context, url, error) => const Icon(Icons.error),
                  imageUrl: avatarUrl
              ),
            ),
            avatarStatusUrl.isNotEmpty ? Positioned(
              right: -realSize/10,
              bottom: -realSize/10,
              child: Container(
                decoration: const BoxDecoration(
                    color: Colors.white,
                    shape: BoxShape.circle
                ),
                padding: EdgeInsets.all(realSize/10),
                child: Image.asset(avatarStatusUrl, width: realSize/3, height: realSize/3,),
              ),
            ) : SizedBox()
          ]
      ),
    );
  }
}