import 'package:cached_network_image/cached_network_image.dart';
import 'package:fbui/constants/resources.dart';
import 'package:fbui/widgets/avatar/avatar.dart';
import 'package:flutter/material.dart';

enum BodyType {
  textImage, textOnly
}

enum HeaderType {
  group, people
}

enum FooterType {
  likeShareComment, likeCommentMessage
}

enum ReactionType {
  like, care, haha, love, angry, wow, sad
}

class PostData {
  PostData({
    required this.footerType, required this.contentColor, required this.headerType, required this.bodyType, required this.imageUrl, required this.avatarUrl,
    required this.title, required this.time, required this.subTitle, required this.reacts, required this.totalComment, required this.content, required this.totalReaction
});

  BodyType bodyType;
  HeaderType headerType;
  FooterType footerType;

  String title;
  String subTitle;
  String time;
  String avatarUrl;
  String content;
  String contentColor;
  String imageUrl;
  String totalReaction;
  List<ReactionType> reacts;
  String totalComment;
}

class PostHeader extends StatelessWidget {
  const PostHeader({required this.postData, Key? key}) : super(key: key);

  final PostData postData;

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.only(left: 15, right: 15, top: 20, bottom: 10),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Avatar(
            avatarUrl: postData.avatarUrl,
            size: AvatarSize.small,
          ),
          const SizedBox(width: 10,),
          Column(
            mainAxisSize: MainAxisSize.min,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(postData.title, overflow: TextOverflow.ellipsis, maxLines: 1, style: Theme.of(context).textTheme.titleMedium?.copyWith(fontWeight: FontWeight.bold),),
              const SizedBox(height: 5,),
              Row(
                mainAxisSize: MainAxisSize.min,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  postData.headerType == HeaderType.group ? Text(postData.subTitle, overflow: TextOverflow.ellipsis, maxLines: 1, style: Theme.of(context).textTheme.caption?.copyWith(color: Colors.black54, fontWeight: FontWeight.bold),) : const SizedBox.shrink(),
                  postData.headerType == HeaderType.group ? Container(
                    width: 3,
                    height: 3,
                    margin: const EdgeInsets.symmetric(horizontal: 10),
                    decoration: const BoxDecoration(
                        color: Colors.grey,
                        shape: BoxShape.circle
                    ),
                  ) : const SizedBox.shrink(),
                  Text(postData.time, overflow: TextOverflow.ellipsis, maxLines: 1, style: Theme.of(context).textTheme.caption?.copyWith(color: Colors.black54),),
                  Container(
                    width: 3,
                    height: 3,
                    margin: const EdgeInsets.symmetric(horizontal: 10),
                    decoration: const BoxDecoration(
                        color: Colors.grey,
                        shape: BoxShape.circle
                    ),
                  ),
                  Image.asset(kImageEarth, width: 12, height: 12, color: Colors.black54,)
                ],
              )
            ],
          ),
          Expanded(child: Container()),
          InkWell(
            onTap: () {},
            child: const Icon(Icons.more_horiz, size: 30, color: Colors.black54,),
          ),
          const SizedBox(width: 10,),
          InkWell(
            onTap: () {},
            child: const Icon(Icons.close, size: 30, color: Colors.black54,),
          ),
          const SizedBox(width: 0,)
        ],
      ),
    );
  }
}

class PostBody extends StatelessWidget {
  const PostBody({Key? key, required this.postData}) : super(key: key);

  final PostData postData;

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisSize: MainAxisSize.min,
      children: [
        const SizedBox(height: 10,),
        postData.bodyType == BodyType.textOnly ? Stack(
          children: [
            Positioned.fill(
              child: Container(
                color: Colors.orange,
              ),
            ),
            Container(
              padding: const EdgeInsets.symmetric(vertical: 30, horizontal: 15),
              child: Text(
                postData.content,
                textAlign: TextAlign.center,
                style: Theme.of(context).textTheme.bodyText2?.copyWith(color: Colors.white, fontWeight: FontWeight.bold, fontSize: 26),
              ),
            ),
          ],
        ) : Container(
          padding: const EdgeInsets.only(left: 10, right: 10, bottom: 10),
          child: Text(
            postData.content,
            style: Theme.of(context).textTheme.bodyText1?.copyWith(color: Colors.black87),
          ),
        ),
        const SizedBox(height: 10,),
        postData.bodyType == BodyType.textImage ? CachedNetworkImage(imageUrl: postData.imageUrl,
          fit: BoxFit.fitWidth,
        ) : const SizedBox.shrink(),
        postData.bodyType == BodyType.textImage ? const SizedBox(height: 10,) : const SizedBox.shrink(),
      ],
    );
  }
}

class PostFooter extends StatelessWidget {

  final PostData postData;

  const PostFooter({Key? key, required this.postData}) : super(key: key);

  List<Widget> _getReaction(BuildContext context) {
    return postData.reacts.map<Widget>((e) {

      switch (e) {
        case ReactionType.angry:
          return Image.asset(kImageAngry, width: 20, height: 20,);
        case ReactionType.haha:
          return Image.asset(kImageHaha, width: 20, height: 20,);
        case ReactionType.love:
          return Image.asset(kImageLove, width: 20, height: 20,);
        case ReactionType.sad:
          return Image.asset(kImageSad, width: 20, height: 20,);
        case ReactionType.wow:
          return Image.asset(kImageVow, width: 20, height: 20,);
        case ReactionType.like:
          return Image.asset(kImageRLike, width: 20, height: 20,);
        case ReactionType.care:
          return Image.asset(kImageCare, width: 20, height: 20,);
      }
    }).toList();
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisSize: MainAxisSize.min,
      children: [
        Container(
          padding: const EdgeInsets.only(left: 10, right: 10),
          child: Row(
            children: _getReaction(context) + [
              const SizedBox(width: 10,),
              Text(postData.totalReaction, style: Theme.of(context).textTheme.bodyText2?.copyWith(color: Colors.black54, fontWeight: FontWeight.bold),),
              Expanded(child: Container()),
              Text("${postData.totalComment} comments", style: Theme.of(context).textTheme.bodyText2?.copyWith(color: Colors.black54, fontWeight: FontWeight.bold),),
              const SizedBox(width: 10,),
            ],
          ),
        ),
        const SizedBox(height: 10,),
        Container(
          height: 1,
          color: Colors.black12,
        ),
        const SizedBox(height: 15,),
        Container(
          padding: const EdgeInsets.only(bottom: 15),
          child: Row(
            children: [
              Expanded(
                child: Container(
                  alignment: Alignment.center,
                  child: Image.asset(kImageRLike, width: 20, height: 20,),
                ),
              ),
              Expanded(
                child: Container(
                  alignment: Alignment.center,
                  child: Image.asset(kImageRComment, width: 20, height: 20,),
                ),
              ),
              postData.footerType == FooterType.likeShareComment ? Expanded(
                child: Container(
                  alignment: Alignment.center,
                  child: Image.asset(kImageRshare, width: 20, height: 20,),
                ),
              ) : const SizedBox.shrink()
            ],
          ),
        ),
      ],
    );
  }
}

class Post extends StatelessWidget {
  const Post({required this.postData, Key? key}) : super(key: key);

  final PostData postData;

  @override
  Widget build(BuildContext context) {
    return Container(
      color: Theme.of(context).scaffoldBackgroundColor,
      width: double.maxFinite,
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: [
          PostHeader(postData: postData,),
          PostBody(postData: postData,),
          PostFooter(postData: postData,),
        ],
      ),
    );
  }
}