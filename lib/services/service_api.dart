import 'package:fbui/constants/apis.dart';
import 'package:fbui/model/post.dart';
import 'package:fbui/services/service_response.dart';
import 'package:get/get.dart';
import '../client/dio_client.dart';

class ServiceApi {
  final DioClient dioClient = Get.find();

  Future<ServiceResponse> getPost() async {
    DioResponse dioResponse = await dioClient.request(kApiGetPost);
    ServiceResponse serviceResponse = ServiceResponse.processJsonArrayResponse(dioResponse, parse: (responses) => responses.map((e) => PostModel.fromJson(e)).toList());
    return serviceResponse;
  }
}