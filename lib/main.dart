import 'package:fbui/controller/post_controller.dart';
import 'package:fbui/controller/story_controller.dart';
import 'package:fbui/pages/home/home.dart';
import 'package:fbui/services/service_api.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';
import 'client/dio_client.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await GetStorage.init();
  Get.put(GetStorage());
  Get.put(DioClient());
  Get.put(ServiceApi());
  Get.put(PostController());
  Get.put(StoryController());
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    Color primary = const Color(0xff4267B2);

    ThemeData theme = ThemeData(
        primaryColor: primary,
        scaffoldBackgroundColor: Colors.white,
        bottomNavigationBarTheme: BottomNavigationBarThemeData(
          backgroundColor: Colors.white,
          type: BottomNavigationBarType.fixed,
          selectedItemColor: primary,
          unselectedItemColor: Colors.grey,
          elevation: 0,
        ),
        tabBarTheme: TabBarTheme(
          labelColor: primary,
          labelStyle: ThemeData.light().textTheme.labelLarge?.copyWith(fontWeight: FontWeight.bold),
          unselectedLabelStyle: ThemeData.light().textTheme.labelLarge?.copyWith(fontWeight: FontWeight.bold),
          indicator: UnderlineTabIndicator(
            borderSide: BorderSide(color: primary, width: 2)
          ),
          unselectedLabelColor: Colors.grey,
        ),
        textTheme: const TextTheme(

        )
    );

    return GetMaterialApp(
      title: 'Facebook Demo',
      theme: theme,
      darkTheme: theme.copyWith(
        scaffoldBackgroundColor: Colors.black26
      ),
      home: const HomeScreen(),
    );
  }
}

