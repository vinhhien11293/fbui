
class PostModel {
  String title;
  String name;
  String time;
  String avatar;
  String color;
  String content;
  String contentImage;
  String totalReact;
  String totalComment;
  List<int> reacts;
  int headType;
  int bodyType;
  int footType;

  PostModel({
    required this.reacts,
    required this.totalComment,
    required this.bodyType,
    required this.time,
    required this.title,
    required this.name,
    required this.avatar,
    required this.contentImage,
    required this.footType,
    required this.headType,
    required this.totalReact,
    required this.content,
    required this.color
  });

  factory PostModel.fromJson(Map<String, dynamic> json) {
    return PostModel(
      name: json["name"],
      time: json["time"],
      title: json["title"],
      avatar: json["avatar"],
      bodyType: json["bodyType"],
      contentImage: json["contentImage"],
      footType: json["footType"],
      headType: json["headType"],
      totalComment: json["totalComment"],
      totalReact: json["totalReact"],
      color: json["color"],
      content: json["content"],
      reacts: (json["reacts"] as List).map((e) => e as int).toList()
    );
  }
}