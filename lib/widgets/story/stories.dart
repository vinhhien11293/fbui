
import 'package:fbui/controller/story_controller.dart';
import 'package:fbui/widgets/story/story_item.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class Stories extends StatelessWidget {
  Stories({Key? key}) : super(key: key);

  final StoryController _storyController = Get.find();

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: const BoxDecoration(
        color: Color(0x80e7e7e7)
      ),
      height: 220,
      child: ListView.builder(
        scrollDirection: Axis.horizontal,
        itemCount: _storyController.rxStories.length,
        itemBuilder: (BuildContext context, int index) => Container(
          margin: const EdgeInsets.symmetric(horizontal: 10),
          alignment: Alignment.center,
          child: StoryItem(
            isMe: _storyController.rxStories[index].isCreator,
            storyName: _storyController.rxStories[index].name,
            storyAvatar: _storyController.rxStories[index].avatarUrl,
            storyImage: _storyController.rxStories[index].imageUrl,
          ),
        ),
      ),
    );
  }
}